﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using static Android.Widget.AdapterView;
using ChatModels;

namespace ChatRoomsMobile.Droid
{
    [Activity(Label = "RoomSelect")]
    public class RoomSelect : Activity
    {
        ListView mListView;
        List<Room> roomList = MainActivity.engine.GetRooms();
        List<string> myList = new List<string>();
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.RoomSelect);
            for (int i = 0; i < roomList.Count; i++)
            {
                myList.Add(roomList[i].Name);
            }
            mListView = FindViewById<ListView>(Resource.Id.lvRooms);
            ArrayAdapter<string> adapter = new ArrayAdapter<string>(this, Resource.Layout.roomItem, myList);
            mListView.Adapter = adapter;
            mListView.ItemClick += (object sender, ItemClickEventArgs e) =>
            {
                ListView lv = (ListView)sender;
                string selectedFromList = (string)lv.GetItemAtPosition(e.Position);     //works
                Intent intent = new Intent(this, typeof(Chat));
                intent.PutExtra("roomName", selectedFromList);
                StartActivity(intent);
            };
        }
    }
}