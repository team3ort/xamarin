﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using static Android.Widget.AdapterView;
using System.Threading;

namespace ChatRoomsMobile.Droid
{
    [Activity(Label = "Chat")]
    public class Chat : Activity
    {
        //List<string> msgList = new List<string>();
        List<string> myList = new List<string>();
        ListView mListView;
        Button sendButton;
        EditText etMessage;
        string roomName;
        Thread thread;
        ArrayAdapter<string> adapter;
        

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Chat);
            sendButton = FindViewById<Button>(Resource.Id.btnSend);
            thread = new Thread(Receive);
            thread.Start();
            roomName = Intent.GetStringExtra("roomName");
            mListView = FindViewById<ListView>(Resource.Id.lvMessages);
            adapter = new ArrayAdapter<string>(this, Resource.Layout.messageItem, myList);
            mListView.Adapter = adapter;
            sendButton.Click += delegate
            {
                etMessage = FindViewById<EditText>(Resource.Id.etMessage);
                MainActivity.engine.SendMessage(etMessage.Text, roomName);
            };
        }

        protected override void OnResume()
        {
            base.OnResume();
            //roomName = Intent.GetStringExtra("roomName");
            MainActivity.engine.GetAllMessageInRoom(roomName);
        }

        public void Receive()
        {
            ChatModels.Message msg;
            string msgStr = "";
            while (true)
            {
                if (!MainActivity.engine.IsQueueEmpty())
                {
                    msg = MainActivity.engine.GetMessageFromQueue();
                    msgStr = msg.Author.Login + ": " + msg.MsgData;
                    RunOnUiThread(() => adapter.Add(msgStr));
                }
            }
        }

    }
}
