﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace ChatRoomsMobile.Droid
{
    [Activity(Label = "ChatRoomsMobile.Droid", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        public static ChatEngine engine;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Login);

            engine = new ChatEngine();
            Button okButton = FindViewById<Button>(Resource.Id.ok);

           
            okButton.Click += delegate
            {
                string login = FindViewById<EditText>(Resource.Id.etLogin).Text;
                string password = FindViewById<EditText>(Resource.Id.etPassword).Text;
                try
                {
                    ChatClient.User = engine.LogIn(login, password);
                }
                catch (ArgumentException ex)
                {
                    // MessageBox.Show(ex.Message);
                    // DisplayAlert("Alert", "Error", "OK");
                }
                Intent intent = new Intent(this, typeof(RoomSelect));
                StartActivity(intent);
            };
        }
    }
}


