using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ChatModels;

namespace ChatRoomsMobile.Droid
{
    public class DroidIntefaceAdapter
    {

        ListView lv;
        //UITextView TextBox;

        public DroidIntefaceAdapter(ListView lv)
        {
            this.lv = lv;
        }
      //  ListView TextBox;

        public string AppendMessageBoard()
        {
            ChatModels.Message msg;
            if (!MainActivity.engine.IsQueueEmpty())
            {
                //msg = MainActivity.engine.GetMessageFromQueue();
                msg = MainActivity.engine.GetMessageFromQueue();
                return msg.Author + ": " + msg.MsgData;
                //TextBox.BeginInvokeOnMainThread(() => TextBox.Text += msg.Author.Login + ": " + msg.MsgData + Environment.NewLine);
            }
            return "";
        }
    }
}