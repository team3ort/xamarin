﻿using System;

using UIKit;
using ChatModels;
using Foundation;

namespace ChatRoomsMobile.iOS
{
    public partial class ViewController : UIViewController
    {

        public ViewController(IntPtr handle) : base(handle)
        {
        }
        

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.
            loginBtn.AccessibilityIdentifier = "myButton";
            txtLogin.AccessibilityIdentifier = "txtLogin";
            txtPassword.AccessibilityIdentifier = "txtPassword";
            loginBtn.TouchUpInside += delegate
            {
                try
                {
                    string login = txtLogin.Text;
                    string password = txtPassword.Text;
                    ChatClient.User = Application.engine.LogIn(login, password);
                    this.PerformSegue("ToRoomSelect", this);
                }
                catch (ArgumentException ex)
                {
                    //Create Alert
                    var okAlertController = UIAlertController.Create("Alert", ex.Message, UIAlertControllerStyle.Alert);

                    //Add Action
                    okAlertController.AddAction(UIAlertAction.Create("Ok", UIAlertActionStyle.Default, null));

                    // Present Alert
                    PresentViewController(okAlertController, true, null);
                }
            };
        }

        //public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        //{
        //    //base.PrepareForSegue(segue, sender);
        //    //if (segue.Identifier == "ToRoomSelect")
        //    //{
        //    //    var destCtrl = segue.DestinationViewController;
        //    //}
        //}


        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

    }
}

