﻿// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace ChatRoomsMobile.iOS
{
    [Register ("TVCMessaging")]
    partial class TVCMessaging
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnSendTo { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField editTextSendTo { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView tableMessaging { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextView textBox { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (btnSendTo != null) {
                btnSendTo.Dispose ();
                btnSendTo = null;
            }

            if (editTextSendTo != null) {
                editTextSendTo.Dispose ();
                editTextSendTo = null;
            }

            if (tableMessaging != null) {
                tableMessaging.Dispose ();
                tableMessaging = null;
            }

            if (textBox != null) {
                textBox.Dispose ();
                textBox = null;
            }
        }
    }
}