﻿using Foundation;
using System;
using System.Threading;
using UIKit;

namespace ChatRoomsMobile.iOS
{
    public partial class TVCMessaging : UITableViewController
    {

        string RoomName = "";
        Thread tread;
        IOSInterfaceAdapter adapter;

        public TVCMessaging (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            //base.ViewDidLoad();
            Application.engine.GetAllMessageInRoom(RoomName);
            adapter = new IOSInterfaceAdapter(textBox);

            btnSendTo.TouchUpInside += BtnSendTo_TouchUpInside;

            tread = new Thread(Reciver);
            tread.Start();
        }

        private void BtnSendTo_TouchUpInside(object sender, EventArgs e)
        {
            Application.engine.SendMessage(editTextSendTo.Text, RoomName);
        }

        public void SetRoomName(string str)
        {
            RoomName = str;
        }

        private void Reciver()
        {
            while (true)
            {
                adapter.AppendMessageBoard();
                Thread.Sleep(1000);
            }
        }


    }
}