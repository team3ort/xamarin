﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;

namespace ChatRoomsMobile.iOS
{
	public class Application
	{
        public static ChatEngine engine;
        // This is the main entry point of the application.
        static void Main (string[] args)
		{
            // if you want to use a different Application Delegate class from "AppDelegate"
            // you can specify it here.
            engine = new ChatEngine();
            UIApplication.Main (args, null, "AppDelegate");           
        }
	}
}
