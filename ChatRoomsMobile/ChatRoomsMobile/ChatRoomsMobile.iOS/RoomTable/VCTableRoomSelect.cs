﻿using Foundation;
using System;
using UIKit;
using ChatModels;
using System.Collections;
using System.Collections.Generic;

namespace ChatRoomsMobile.iOS
{
    public partial class VCTableRoomSelect : UITableViewController
    {
        List<Room> rooms;
        List<string> roomsNames;
        
        public VCTableRoomSelect (IntPtr handle) : base (handle)
        {
            
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            rooms = Application.engine.GetRooms();
            roomsNames = new List<string>();
            for (int i = 0; i < rooms.Count; i++)
            {
                roomsNames.Add(rooms[i].Name);
            }
            TableRoomSource source = new TableRoomSource(roomsNames);
            this.TableView.Source = source;
        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);

            var cell = (UITableViewCell)sender;
            string roomName = cell.TextLabel.Text;

            var dest = (TVCMessaging) segue.DestinationViewController;
            dest.SetRoomName(roomName);
        }

        //public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        //{
        //    //base.RowSelected(tableView, indexPath);
        //    this.PerformSegue("SegueToRoom", this);
        //}
    }
}