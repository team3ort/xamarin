using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Foundation;
using UIKit;

namespace ChatRoomsMobile.iOS
{
    public class TableRoomSource : UITableViewSource
    {

        List<string> items;
        String cellIdentifier = "TableCell";

        public TableRoomSource (List<string> items)
        {
            this.items = items;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            UITableViewCell cell = tableView.DequeueReusableCell(cellIdentifier);

            cell.TextLabel.Text = items[indexPath.Row];

            return cell;            
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return items.Count;
        }
    }
}