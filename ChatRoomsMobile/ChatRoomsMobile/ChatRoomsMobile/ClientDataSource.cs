﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using ChatModels;

namespace ChatRoomsMobile
{
    public static class ClientDataSource
    {
        static Queue<Message> msgs = new Queue<Message>();

        public static Queue<Message> Msgs
        {
            get
            {
                return msgs;
            }

            set
            {
                msgs = value;
            }
        }

    }
}
