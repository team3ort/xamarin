﻿using Sockets.Plugin;
using System;
using System.Net;
using ChatModels;
using System.IO;
using System.Threading;

namespace ChatRoomsMobile
{
    public static class ChatClient
    {
        public static TcpSocketClient client;

        static Stream readStream;
        static Stream writeStream;

        public static User User { get; set; }

        public static async void InitClient(string host, int port)
        {
            User = new User();
            client = new TcpSocketClient();
            await client.ConnectAsync(host, port);
            writeStream = client.WriteStream;
            readStream = client.ReadStream;
        }

        public static void SendRequest(string request)
        {
            BinaryWriter writer = new BinaryWriter(writeStream);
            writer.Write(request);
            //writer.Flush();

        }

        public static string GetResponseForRequest(string request)
        {
            BinaryReader reader = new BinaryReader(readStream);
            SendRequest(request);
            string result = "";
            result = reader.ReadString();
            return result;
        }

        public static string GetResponse()
        {
            BinaryReader reader = new BinaryReader(writeStream);
            string result = "";
            result = reader.ReadString();
            return result;
        }
    }
}

