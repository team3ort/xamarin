﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChatModels;
using Newtonsoft.Json;

namespace ChatRoomsMobile
{
    public class ReceiveResponseParser
    {
        //InterfaceAdapter 
        public void DoWork(ResponsePackage response)
        {
            switch (response.DataType)
            {
                case "message":
                    ChatModels.Message msg = JsonConvert.DeserializeObject<ChatModels.Message>(response.Data);
                    ClientDataSource.Msgs.Enqueue(msg);
                    //adapter.AppendTextSafe(msg.Author.Login + ": " + msg.MsgData + Environment.NewLine);
                    break;
                case "roomlist":
                    List<Room> rooms = JsonConvert.DeserializeObject<List<Room>>(response.Data);
                    //adapter.ClearRoomsSafe();
                    //adapter.AddRoomsSafe(rooms);
                    break;
                case "usersinroom":
                    List<User> users = JsonConvert.DeserializeObject<List<User>>(response.Data);
                    //adapter.ClearUsersSafe();
                    //adapter.AddUsersSafe(users);
                    break;
                case "messageinroom":
                    List<ChatModels.Message> initMessages = JsonConvert.DeserializeObject<List<ChatModels.Message>>(response.Data);
                    for (int i = 0; i < initMessages.Count; i++)
                    {
                        //adapter.AppendTextSafe(initMessages[i].Author.Login + ": " + initMessages[i].MsgData + Environment.NewLine);
                        ClientDataSource.Msgs.Enqueue(initMessages[i]);
                    }
                    break;
                case "rooms":
                    List<Room> res = JsonConvert.DeserializeObject<List<Room>>(response.Data);
                    {
                        //adapter.ClearRoomsSafe();
                        //adapter.AddRoomsSafe(res);
                    }
                    break;
                case "userleaveroom":
                    bool leaveRes = JsonConvert.DeserializeObject<bool>(response.Data);
                    //ClearRoomsSafe();
                    if (leaveRes)
                    {
                        //adapter.ClearUsersSafe();
                        //adapter.ClearMessageSafe();
                    }
                    break;
            }
        }
    }
}
