﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChatModels;

namespace ChatRoomsMobile
{
    public class ChatEngine
    {

        public bool IsQueueEmpty()
        {
            return ClientDataSource.Msgs.Count > 0 ? false : true;
        }
        public Message GetMessageFromQueue()
        {
            return ClientDataSource.Msgs.Dequeue();
        }

        public ChatEngine(string host = "192.168.4.112", int port = 11155)
        {
            ChatClient.InitClient(host, port);
        }

        public bool _isMessaging = false;
        AreModule areModule = new AreModule();
        MessageModule msgModule = new MessageModule();
        RoomManagerModule rmModule = new RoomManagerModule();
        //MessageModule msgModule;
        //RoomMenagerModule rmModule;



        public User LogIn(string login, string password)
        {
            User loginUser = new User(login, password);
            return areModule.LogIn(loginUser);
        }

        public List<Room> GetRooms()
        {
            return rmModule.GetRooms();
        }

        public void GetAllMessageInRoom(string roomName)
        {
            msgModule.GetAllMessagesInRoom(roomName);
        }

        //public bool Register(string login, string password, string email,
        //                     string fName, string lName, string phone)
        //{
        //    User regUser = new User(login, password, fName, lName, email, phone);
        //    return areModule.Register(regUser);
        //}

        //public bool Forget(string email)
        //{
        //    return areModule.Forget(email);
        //}


        //public void InitRoomMenagerModule()
        //{
        //    rmModule = new RoomMenagerModule();
        //}

        //public void GetRooms()
        //{
        //    rmModule.GetRooms();
        //}

        //public void StartMessaging()
        //{
        //    if (!_isMessaging)
        //    {
        //        msgModule.Run();
        //        _isMessaging = true;
        //    }
        //}

        //public void StopMessaging()
        //{
        //    if (_isMessaging)
        //    {
        //        msgModule.Stop();
        //        _isMessaging = false;
        //    }
        //}

        //public void EnterInRoom(Room room)
        //{
        //    rmModule.EnterInRoom(room);
        //}

        //public void LeaveRoom(Room room)
        //{
        //    rmModule.LeaveRoom(room);
        //}

        public void SendMessage(string message, string roomName)
        {
            msgModule.SendMessage(message, roomName);
        }

        //public void CreateRoom(Room room)
        //{
        //    rmModule.CreateRoom(room);
        //}
    }
}
