﻿using ChatModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatRoomsMobile
{
    public class CmdRoomSelect
    {
        string roomName;
        Dictionary<string, string> request;
        public CmdRoomSelect(string  roomName)
        {
            this.roomName = roomName;
        }

        public void SendRequest()
        {
            request = new Dictionary<string, string>();
            request.Add("cmd", "userenterinroom");
            request.Add("room", roomName);

            string reqString = JsonConvert.SerializeObject(request);
            ChatClient.SendRequest(reqString);

        }
    }
}
