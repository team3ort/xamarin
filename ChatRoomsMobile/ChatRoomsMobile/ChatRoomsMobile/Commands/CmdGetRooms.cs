﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChatModels;

namespace ChatRoomsMobile
{
    public class CmdGetRooms
    {
        Dictionary<string, string> request;
        public CmdGetRooms()
        {

        }

        public List<Room> GetResponse()
        {
            request = new Dictionary<string, string>();
            request.Add("cmd", "getallrooms");

            string reqString = JsonConvert.SerializeObject(request);
            string respString = ChatClient.GetResponseForRequest(reqString);
            ResponsePackage rp = JsonConvert.DeserializeObject<ResponsePackage>(respString);
            if (rp.DataType == "rooms")
            {
                List<Room> res = JsonConvert.DeserializeObject<List<Room>>(rp.Data);
                return res;
            }
            else if (rp.DataType == "error")
            {
                Error er = JsonConvert.DeserializeObject<Error>(rp.Data);
                throw new ArgumentException(er.Message);
            }
            throw new ArgumentException("Something wrong");

        }
    }
}
