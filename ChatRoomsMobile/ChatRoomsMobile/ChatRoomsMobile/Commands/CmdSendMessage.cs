﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatRoomsMobile
{
    public class CmdSendMessage
    {
        Dictionary<string, string> request;
        string roomName;
        string message;

        public CmdSendMessage(string roomName, string msg)
        {
            this.roomName = roomName;
            this.message = msg;
        }

        public void SendRequest()
        {
            request = new Dictionary<string, string>();
            request.Add("cmd", "message");
            request.Add("author", ChatClient.User.Login);
            request.Add("roomname", roomName);
            request.Add("msgdata", message);

            string reqString = JsonConvert.SerializeObject(request);
           ChatClient.SendRequest(reqString);
        }
    }
}
