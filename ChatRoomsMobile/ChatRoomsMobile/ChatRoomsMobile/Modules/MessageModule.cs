﻿using ChatModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ChatRoomsMobile
{
    public class MessageModule
    {
        public void GetAllMessagesInRoom(string roomName)
        {
            CmdRoomSelect cmd = new CmdRoomSelect(roomName);
            cmd.SendRequest();
            Task.Run(Receive);
        }

        public async Task Receive()
        {
            await Task.Run(new Action(DoAction));
        }

        public void DoAction()
        {
            ResponsePackage rp = new ResponsePackage();
            ReceiveResponseParser rrp = new ReceiveResponseParser();
            Stream stream = ChatClient.client.ReadStream;
            BinaryReader reader = new BinaryReader(stream);
            while (true)
            {
                rp = JsonConvert.DeserializeObject<ResponsePackage>(reader.ReadString());
                rrp.DoWork(rp);
            }
        }

        public void SendMessage(string message, string roomName)
        {
            CmdSendMessage cmd = new CmdSendMessage(roomName, message);
            cmd.SendRequest();
        }

    }
}
