﻿using ChatModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatRoomsMobile
{
    public class RoomManagerModule
    {
        public List<Room> GetRooms()
        {
            CmdGetRooms cmd = new CmdGetRooms();
            return cmd.GetResponse();
        }
    }
}
