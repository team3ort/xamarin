﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChatModels;

namespace ChatRoomsMobile
{
    public class AreModule
    {
        public User LogIn(User user)
        {
            CmdLogin cmd = new CmdLogin(user);
            User res = cmd.GetResponse();
            ChatClient.User = res;
            return res;
        }

        //public bool Register(User user)
        //{
        //    CmdRegister cmd = new CmdRegister(user);
        //    bool res = cmd.GetResponse();
        //    return res;
        //}

        //public bool Forget(string email)
        //{
        //    CmdForget cmd = new CmdForget(email);
        //    bool res = cmd.GetResponse();
        //    return res;
        //}

        //public bool LogOut(User user)
        //{
        //    return false;
        //}

    }
}
